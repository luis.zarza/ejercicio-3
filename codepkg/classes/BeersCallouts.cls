public class BeersCallouts {   
	public static HttpResponse makeGetCallout() {
        Http http = new Http();
    	HttpRequest request = new HttpRequest();
		request.setEndpoint('https://api.punkapi.com/v2/beers');
    	request.setMethod('GET');
    	HttpResponse response = http.send(request);
        List<Beer__c> theBeers = new List<Beer__c>();          
        if (response.getStatusCode() == 200) {
            List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());            
            for (Object beer: results) {
                Map<String, Object> selectedBeer = (Map<String, Object>)beer;
                List<Beer__c> results2 = [SELECT External_Id__c FROM Beer__c WHERE External_Id__c = :integer.valueof(selectedBeer.get('id'))];
                if(results2.size() == 0){
                	Beer__c newBeer = new Beer__c();
                	newBeer.External_Id__c  = integer.valueof(selectedBeer.get('id'));
                	newBeer.Name = String.valueOf(selectedBeer.get('name'));
                	newBeer.Tagline__c = String.valueOf(selectedBeer.get('tagline'));
                	newBeer.Description__c = String.valueOf(selectedBeer.get('description'));
     				theBeers.add(newBeer);
                	}
                }            	
            }
        if (theBeers.size() > 0){
             Database.insert(theBeers); 
        }     	
        return response;
        }
    }